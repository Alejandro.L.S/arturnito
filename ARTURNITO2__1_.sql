-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 01-09-2020 a las 20:30:05
-- Versión del servidor: 5.7.31-0ubuntu0.18.04.1
-- Versión de PHP: 7.2.24-0ubuntu0.18.04.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ARTURNITO2`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `CONTROL_TURNOS`
--

CREATE TABLE `CONTROL_TURNOS` (
  `Id_Control` smallint(10) NOT NULL,
  `Id_Modulo` smallint(10) NOT NULL,
  `Fecha_Primer_Turno` datetime NOT NULL,
  `Max_Num_Turno` smallint(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELACIONES PARA LA TABLA `CONTROL_TURNOS`:
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `DIA`
--

CREATE TABLE `DIA` (
  `Id_Dia` smallint(10) NOT NULL,
  `Dia` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELACIONES PARA LA TABLA `DIA`:
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ESTADO`
--

CREATE TABLE `ESTADO` (
  `Id_Estado` smallint(10) NOT NULL,
  `Estado` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELACIONES PARA LA TABLA `ESTADO`:
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `FRANJA_HORARIA`
--

CREATE TABLE `FRANJA_HORARIA` (
  `Id_Franja_Horaria` smallint(10) NOT NULL,
  `Inicio_Franja` int(10) NOT NULL,
  `Fin _Franja` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELACIONES PARA LA TABLA `FRANJA_HORARIA`:
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `HORARIO`
--

CREATE TABLE `HORARIO` (
  `Id_Horario` smallint(10) NOT NULL,
  `Id_Dia` smallint(10) NOT NULL,
  `Id_Franja_Horaria` smallint(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELACIONES PARA LA TABLA `HORARIO`:
--   `Id_Dia`
--       `DIA` -> `Id_Dia`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `MODULO`
--

CREATE TABLE `MODULO` (
  `Id_Modulo` smallint(10) NOT NULL,
  `Modulo` varchar(100) NOT NULL,
  `Cod_Modulo` varchar(100) NOT NULL,
  `Horario_Atencion` smallint(10) NOT NULL,
  `Estado_Modulo` smallint(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELACIONES PARA LA TABLA `MODULO`:
--   `Estado_Modulo`
--       `ESTADO` -> `Id_Estado`
--   `Horario_Atencion`
--       `FRANJA_HORARIA` -> `Id_Franja_Horaria`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `PANTALLA`
--

CREATE TABLE `PANTALLA` (
  `Id_Pantalla` smallint(10) NOT NULL,
  `Id_Turno` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELACIONES PARA LA TABLA `PANTALLA`:
--   `Id_Turno`
--       `TURNO` -> `Id_Turno`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `PUESTO`
--

CREATE TABLE `PUESTO` (
  `Id_Puesto` smallint(10) NOT NULL,
  `Puesto` varchar(100) NOT NULL,
  `Estado_puesto` smallint(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELACIONES PARA LA TABLA `PUESTO`:
--   `Estado_puesto`
--       `ESTADO` -> `Id_Estado`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `PUESTOS_TOMADOS`
--

CREATE TABLE `PUESTOS_TOMADOS` (
  `Id` smallint(10) NOT NULL,
  `Id_Usuario` smallint(10) NOT NULL,
  `Id_Puesto` smallint(10) NOT NULL,
  `Id_Horario` smallint(10) NOT NULL,
  `Datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELACIONES PARA LA TABLA `PUESTOS_TOMADOS`:
--   `Id_Usuario`
--       `USUARIO` -> `Id_Usuario`
--   `Id_Puesto`
--       `PUESTO` -> `Id_Puesto`
--   `Id_Horario`
--       `FRANJA_HORARIA` -> `Id_Franja_Horaria`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `TURNO`
--

CREATE TABLE `TURNO` (
  `Id_Turno` int(10) NOT NULL,
  `Id_Modulo` smallint(10) NOT NULL,
  `Turno` varchar(100) NOT NULL,
  `Hora_Turno` time NOT NULL,
  `Id_Estado` smallint(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELACIONES PARA LA TABLA `TURNO`:
--   `Id_Estado`
--       `ESTADO` -> `Id_Estado`
--   `Id_Modulo`
--       `MODULO` -> `Id_Modulo`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `TURNOS_TOMADOS`
--

CREATE TABLE `TURNOS_TOMADOS` (
  `id_TTomados` int(10) NOT NULL,
  `Id_Puesto` smallint(10) NOT NULL,
  `Id_Pantalla` smallint(10) NOT NULL,
  `Id_Estado` smallint(10) NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELACIONES PARA LA TABLA `TURNOS_TOMADOS`:
--   `Id_Pantalla`
--       `PANTALLA` -> `Id_Pantalla`
--   `Id_Puesto`
--       `PUESTOS_TOMADOS` -> `Id`
--   `Id_Estado`
--       `ESTADO` -> `Id_Estado`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `USUARIO`
--

CREATE TABLE `USUARIO` (
  `Id_Usuario` smallint(10) NOT NULL,
  `Nombre` varchar(100) NOT NULL,
  `Apellido` varchar(100) NOT NULL,
  `Cod_Usuario` varchar(100) NOT NULL,
  `Contrasenia` varchar(100) NOT NULL,
  `Cod_Modulo` smallint(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELACIONES PARA LA TABLA `USUARIO`:
--   `Cod_Modulo`
--       `MODULO` -> `Id_Modulo`
--

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `CONTROL_TURNOS`
--
ALTER TABLE `CONTROL_TURNOS`
  ADD PRIMARY KEY (`Id_Control`),
  ADD KEY `Id_Modulo` (`Id_Modulo`);

--
-- Indices de la tabla `DIA`
--
ALTER TABLE `DIA`
  ADD PRIMARY KEY (`Id_Dia`);

--
-- Indices de la tabla `ESTADO`
--
ALTER TABLE `ESTADO`
  ADD PRIMARY KEY (`Id_Estado`);

--
-- Indices de la tabla `FRANJA_HORARIA`
--
ALTER TABLE `FRANJA_HORARIA`
  ADD PRIMARY KEY (`Id_Franja_Horaria`);

--
-- Indices de la tabla `HORARIO`
--
ALTER TABLE `HORARIO`
  ADD PRIMARY KEY (`Id_Horario`),
  ADD KEY `Id_Franja_Horaria` (`Id_Franja_Horaria`),
  ADD KEY `Id_Dia` (`Id_Dia`);

--
-- Indices de la tabla `MODULO`
--
ALTER TABLE `MODULO`
  ADD PRIMARY KEY (`Id_Modulo`),
  ADD KEY `Estado_Modulo` (`Estado_Modulo`),
  ADD KEY `Horario_Atencion` (`Horario_Atencion`);

--
-- Indices de la tabla `PANTALLA`
--
ALTER TABLE `PANTALLA`
  ADD PRIMARY KEY (`Id_Pantalla`),
  ADD KEY `Id_Turno` (`Id_Turno`);

--
-- Indices de la tabla `PUESTO`
--
ALTER TABLE `PUESTO`
  ADD PRIMARY KEY (`Id_Puesto`),
  ADD KEY `Estado_puesto` (`Estado_puesto`);

--
-- Indices de la tabla `PUESTOS_TOMADOS`
--
ALTER TABLE `PUESTOS_TOMADOS`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `Id_Usuario` (`Id_Usuario`),
  ADD KEY `Id_Puesto` (`Id_Puesto`),
  ADD KEY `Id_Horario` (`Id_Horario`);

--
-- Indices de la tabla `TURNO`
--
ALTER TABLE `TURNO`
  ADD PRIMARY KEY (`Id_Turno`),
  ADD KEY `Id_Estado_Turno` (`Id_Estado`),
  ADD KEY `Id_Modulo` (`Id_Modulo`);

--
-- Indices de la tabla `TURNOS_TOMADOS`
--
ALTER TABLE `TURNOS_TOMADOS`
  ADD PRIMARY KEY (`id_TTomados`),
  ADD KEY `Id_Puesto` (`Id_Puesto`),
  ADD KEY `Id_Turno` (`Id_Pantalla`),
  ADD KEY `Id_Estado` (`Id_Estado`);

--
-- Indices de la tabla `USUARIO`
--
ALTER TABLE `USUARIO`
  ADD PRIMARY KEY (`Id_Usuario`),
  ADD KEY `Cod_Modulo` (`Cod_Modulo`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `CONTROL_TURNOS`
--
ALTER TABLE `CONTROL_TURNOS`
  MODIFY `Id_Control` smallint(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `DIA`
--
ALTER TABLE `DIA`
  MODIFY `Id_Dia` smallint(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `ESTADO`
--
ALTER TABLE `ESTADO`
  MODIFY `Id_Estado` smallint(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `FRANJA_HORARIA`
--
ALTER TABLE `FRANJA_HORARIA`
  MODIFY `Id_Franja_Horaria` smallint(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `HORARIO`
--
ALTER TABLE `HORARIO`
  MODIFY `Id_Horario` smallint(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `MODULO`
--
ALTER TABLE `MODULO`
  MODIFY `Id_Modulo` smallint(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `PANTALLA`
--
ALTER TABLE `PANTALLA`
  MODIFY `Id_Pantalla` smallint(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `PUESTO`
--
ALTER TABLE `PUESTO`
  MODIFY `Id_Puesto` smallint(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `PUESTOS_TOMADOS`
--
ALTER TABLE `PUESTOS_TOMADOS`
  MODIFY `Id` smallint(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `TURNO`
--
ALTER TABLE `TURNO`
  MODIFY `Id_Turno` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `TURNOS_TOMADOS`
--
ALTER TABLE `TURNOS_TOMADOS`
  MODIFY `id_TTomados` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `USUARIO`
--
ALTER TABLE `USUARIO`
  MODIFY `Id_Usuario` smallint(10) NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `HORARIO`
--
ALTER TABLE `HORARIO`
  ADD CONSTRAINT `HORARIO_ibfk_1` FOREIGN KEY (`Id_Dia`) REFERENCES `DIA` (`Id_Dia`);

--
-- Filtros para la tabla `MODULO`
--
ALTER TABLE `MODULO`
  ADD CONSTRAINT `MODULO_ibfk_1` FOREIGN KEY (`Estado_Modulo`) REFERENCES `ESTADO` (`Id_Estado`),
  ADD CONSTRAINT `MODULO_ibfk_2` FOREIGN KEY (`Horario_Atencion`) REFERENCES `FRANJA_HORARIA` (`Id_Franja_Horaria`);

--
-- Filtros para la tabla `PANTALLA`
--
ALTER TABLE `PANTALLA`
  ADD CONSTRAINT `PANTALLA_ibfk_2` FOREIGN KEY (`Id_Turno`) REFERENCES `TURNO` (`Id_Turno`);

--
-- Filtros para la tabla `PUESTO`
--
ALTER TABLE `PUESTO`
  ADD CONSTRAINT `PUESTO_ibfk_3` FOREIGN KEY (`Estado_puesto`) REFERENCES `ESTADO` (`Id_Estado`);

--
-- Filtros para la tabla `PUESTOS_TOMADOS`
--
ALTER TABLE `PUESTOS_TOMADOS`
  ADD CONSTRAINT `PUESTOS_TOMADOS_ibfk_2` FOREIGN KEY (`Id_Usuario`) REFERENCES `USUARIO` (`Id_Usuario`),
  ADD CONSTRAINT `PUESTOS_TOMADOS_ibfk_3` FOREIGN KEY (`Id_Puesto`) REFERENCES `PUESTO` (`Id_Puesto`),
  ADD CONSTRAINT `PUESTOS_TOMADOS_ibfk_4` FOREIGN KEY (`Id_Horario`) REFERENCES `FRANJA_HORARIA` (`Id_Franja_Horaria`);

--
-- Filtros para la tabla `TURNO`
--
ALTER TABLE `TURNO`
  ADD CONSTRAINT `TURNO_ibfk_1` FOREIGN KEY (`Id_Estado`) REFERENCES `ESTADO` (`Id_Estado`),
  ADD CONSTRAINT `TURNO_ibfk_2` FOREIGN KEY (`Id_Modulo`) REFERENCES `MODULO` (`Id_Modulo`);

--
-- Filtros para la tabla `TURNOS_TOMADOS`
--
ALTER TABLE `TURNOS_TOMADOS`
  ADD CONSTRAINT `TURNOS_TOMADOS_ibfk_2` FOREIGN KEY (`Id_Pantalla`) REFERENCES `PANTALLA` (`Id_Pantalla`),
  ADD CONSTRAINT `TURNOS_TOMADOS_ibfk_3` FOREIGN KEY (`Id_Puesto`) REFERENCES `PUESTOS_TOMADOS` (`Id`),
  ADD CONSTRAINT `TURNOS_TOMADOS_ibfk_4` FOREIGN KEY (`Id_Estado`) REFERENCES `ESTADO` (`Id_Estado`);

--
-- Filtros para la tabla `USUARIO`
--
ALTER TABLE `USUARIO`
  ADD CONSTRAINT `USUARIO_ibfk_1` FOREIGN KEY (`Cod_Modulo`) REFERENCES `MODULO` (`Id_Modulo`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
